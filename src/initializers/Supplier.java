package initializers;
import controllers.ClientController;
import controllers.OwnerController;
import models.CashBox;
import models.Client;
import models.Vehicle;
import services.AccountantService;
import services.CashBoxService;
import services.ColumnService;
import views.ClientUI;
import views.OwnerUI;

public class Supplier {
	public Container initalizeObjects() {
		// models
		Vehicle vehicle = new Vehicle("Audi");
		Client client = new Client("Иван", 40, vehicle);
		CashBox cashBox = new CashBox();
	
		// UI
		OwnerUI ownerUI = new OwnerUI();
		ClientUI clientUI = new ClientUI();
		
		// controllers
		OwnerController ownerController = new OwnerController();
		ClientController clientController = new ClientController(client);
		
		// services
		AccountantService accountantService = new AccountantService();
		CashBoxService cashBoxService = new CashBoxService();
		ColumnService columnService = new ColumnService();
		
		return new Container(cashBox, ownerUI, clientUI, ownerController, clientController, cashBoxService, columnService, accountantService);		
	}
}
