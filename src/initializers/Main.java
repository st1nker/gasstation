package initializers;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class Main {
	static Container container = (new Supplier()).initalizeObjects();
	
	public static void main(String[] args) {
		Display display = new Display();
		container.setDisplay(display);
		Shell shell = new Shell(display);
		
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.marginLeft = 50;
		layout.marginRight = 50;
		layout.marginTop = 30;
		layout.marginBottom = 50;
		
		shell.setLayout(layout);

		
		Label label = new Label(shell, SWT.BORDER);
		label.setLayoutData(new GridData());
		FontData[] fD = label.getFont().getFontData();
		fD[0].setHeight(16);
		label.setFont( new Font(display,fD[0]));
		label.setText("Автозаправочная станция");
		
		Button buttonEnterAsOwner = new Button(shell, SWT.NONE);
		GridData gridDataOfButtonEnterAsOwner = new GridData(200, 50);
		gridDataOfButtonEnterAsOwner.horizontalAlignment = SWT.CENTER;
		gridDataOfButtonEnterAsOwner.verticalIndent = 100;
		buttonEnterAsOwner.setLayoutData(gridDataOfButtonEnterAsOwner);
		buttonEnterAsOwner.setText("Войти как владелец");
		
		buttonEnterAsOwner.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				enterAsOwner();
	        }
	    });
		
		Button buttonEnterAsClient = new Button(shell, SWT.NONE);
		GridData gridDataOfButtonEnterAsClient = new GridData(200, 50);
		gridDataOfButtonEnterAsClient.horizontalAlignment = SWT.CENTER;
		gridDataOfButtonEnterAsClient.verticalIndent = 10;
		buttonEnterAsClient.setLayoutData(gridDataOfButtonEnterAsClient);
		buttonEnterAsClient.setText("Войти как клиент");	

        buttonEnterAsClient.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
            	enterAsClient();
            }
        });
		
		shell.pack();
		shell.open();
		
		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) display.sleep ();
		}
		display.dispose ();
	}
	
	public static void enterAsOwner() {
		container.getOwnerUI().createInterface();
		
	}
	
	public static void enterAsClient() {
		container.getClientUI().createInterface();
	}
}
