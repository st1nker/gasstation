package initializers;

import org.eclipse.swt.widgets.Display;

import controllers.ClientController;
import controllers.OwnerController;
import models.CashBox;
import services.AccountantService;
import services.CashBoxService;
import services.ColumnService;
import views.ClientUI;
import views.OwnerUI;

public class Container {
	protected Display display;
	protected CashBox cashBox;
	protected OwnerUI ownerUI;
	protected ClientUI clientUI;
	protected OwnerController ownerController;
	protected ClientController clientController;
	protected CashBoxService cashBoxService;
	protected ColumnService columnService;
	protected AccountantService accountantService;

	public Container(CashBox cashBox, OwnerUI ownerUI, ClientUI clientUI, OwnerController ownerController,
			ClientController clientController, CashBoxService cashBoxService, ColumnService columnService,
			AccountantService accountantService) {
		this.cashBox = cashBox;
		this.ownerUI = ownerUI;
		this.ownerUI.setContainer(this);
		this.clientUI = clientUI;
		this.clientUI.setContainer(this);
		this.ownerController = ownerController;
		this.ownerController.setContainer(this);
		this.clientController = clientController;
		this.clientController.setContainer(this);
		this.cashBoxService = cashBoxService;
		this.cashBoxService.setContainer(this);
		this.columnService = columnService;
		this.columnService.setContainer(this);
		this.accountantService = accountantService;
		this.accountantService.setContainer(this);
	}
	
	public Display getDisplay() {
		return display;
	}

	public void setDisplay(Display display) {
		this.display = display;
	}

	public CashBox getCashBox() {
		return cashBox;
	}

	public OwnerUI getOwnerUI() {
		return ownerUI;
	}

	public ClientUI getClientUI() {
		return clientUI;
	}

	public OwnerController getOwnerController() {
		return ownerController;
	}

	public ClientController getClientController() {
		return clientController;
	}

	public CashBoxService getCashBoxService() {
		return cashBoxService;
	}

	public ColumnService getColumnService() {
		return columnService;
	}

	public AccountantService getAccountantService() {
		return accountantService;
	}
}
