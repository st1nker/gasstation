package models;

import java.util.ArrayList;
import java.util.List;

public class Client {
	String name;
	int money;
	Vehicle vehicle;
	List<Product> clientProducts = new ArrayList<>();
	List<Fuel> clientFuel = new ArrayList<>();
	
	public Client(String name, int money, Vehicle vehicle) {
		this.name = name;
		this.money = money;
		this.vehicle = vehicle;
	}

	public List<Product> getClientProducts() {
		return clientProducts;
	}
	
	public List<Fuel> getClientFuel() {
		return clientFuel;
	}
	
	public String getName() {
		return name;
	}
	
	public int getMoney() {
		return money;
	}
	
	public Vehicle getVehicle() {
		return vehicle;
	}
	
	public void setMoney(int money) {
		this.money = money;
	}
}
