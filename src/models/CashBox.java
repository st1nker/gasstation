package models;

import java.util.ArrayList;
import java.util.List;

public class CashBox {
	int earningsForProducts;
	int earningsForFuel;
	List<Product> products = new ArrayList<>();
	List<Fuel> fuel = new ArrayList<>();
	
	public CashBox() {
		earningsForProducts = 0;
		earningsForFuel = 0;
	}
	
	public List<Product> getProducts() {
		return products;
	}

	public List<Fuel> getFuel() {
		return fuel;
	}
	
	public int getEarningsForProducts() {
		return earningsForProducts;
	}

	public int getEarningsForFuel() {
		return earningsForFuel;
	}
	
	public void setEarningsForProducts(int earningsForProducts) {
		this.earningsForProducts = earningsForProducts;
	}
	
	public void setEarningsForFuel(int earningsForFuel) {
		this.earningsForFuel = earningsForFuel;
	}
}
