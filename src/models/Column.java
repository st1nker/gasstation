package models;

import java.util.ArrayList;
import java.util.List;

public class Column {
	int number;
	boolean close;
	List<Fuel> allFuel = new ArrayList<>();
	
	public Column(int number) {
		close = false;
		this.number = number;
	}
	
	public int getNumber() {
		return number;
	}

	public boolean isClose() {
		return close;
	}

	public List<Fuel> getAllFuel() {
		return allFuel;
	}
	
	public void setClose(boolean close) {
		this.close = close;
	}
}
