package models;

public class Vehicle {
	String carModel;
	int tankFullness;
	
	public Vehicle(String carModel) {
		this.carModel = carModel;
		tankFullness = 0;
	}
	
	public int getTankFullness() {
		return tankFullness;
	}
	
	public void setTankFullness(int tankFullness) {
		this.tankFullness = tankFullness;
	}
}
