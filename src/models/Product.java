package models;

public class Product {
	String name;
	int price;
	int number;
	ProductType type;
	
	public Product(String name, int number, int price, ProductType type) {
		this.name = name;
		this.number = number;
		this.price = price;
		this.type = type;
	}
	
	public int getPrice() {
		return price;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getName() {
		return name;
	};

	public int getСost() {
		return price;
	}
	
	public ProductType getType() {
		return type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setType(ProductType type) {
		this.type = type;
	}
}
