package models;

public class Fuel {
	String name;
	int number;
	int price;
	FuelType type;
	
	public Fuel(String name, int number, int price, FuelType type) {
		this.name = name;
		this.number = number;
		this.price = price;
		this.type = type;
	}
	
	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}


	public String getName() {
		return name;
	};
	
	public int getPrice() {
		return price;
	}
	
	public void setType(FuelType type) {
		this.type = type;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}

	public FuelType getType() {
		return type;
	}

}
