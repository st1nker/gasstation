package services;

import java.util.List;

import initializers.Container;
import models.Client;
import models.Column;
import models.Fuel;
import models.Product;

public class CashBoxService {
	Container container;
	
	public void setContainer(Container container) {
		this.container = container;
	}

	public Product sellProduct(Product product, int number) {
		Client client = container.getClientController().getClient();
		int clientMoney = client.getMoney() - product.getPrice() * number;
		client.setMoney(clientMoney);
		
		List<Product> allProducts = client.getClientProducts();
		boolean foundSimilar = false;
		Product existingClientProduct = null;
		
		for (Product clientProduct : allProducts) {
			if (clientProduct.getName().equals(product.getName()) && product.getType() == product.getType()) {
				clientProduct.setNumber(clientProduct.getNumber() + number);
				product.setNumber(product.getNumber() - number);
				foundSimilar = true;
				existingClientProduct = clientProduct;
			}
		}
		if (foundSimilar) {
			return existingClientProduct;
		}
		
		Product newClientProduct = new Product(product.getName(), number, product.getPrice(), product.getType());
		allProducts.add(newClientProduct);
		container.getCashBox().setEarningsForProducts(container.getCashBox().getEarningsForProducts() + product.getPrice() * number);
		return newClientProduct;
	}

	public Fuel sellFuel(Column column, Fuel fuel, int number) {
		Client client = container.getClientController().getClient();
		int clientMoney = client.getMoney() - fuel.getPrice() * number;
		client.setMoney(clientMoney);
		
		List<Fuel> allColumnFuel = column.getAllFuel();
		boolean foundSimilar = false;
		for (Fuel columnFuel : allColumnFuel) {
			if (columnFuel.getName().equals(fuel.getName()) && columnFuel.getType() == fuel.getType()) {
				columnFuel.setNumber(columnFuel.getNumber() + number);
				fuel.setNumber(fuel.getNumber() - number);
				foundSimilar = true;
			}
		}
		
		if (!foundSimilar) {
			allColumnFuel.add(new Fuel(fuel.getName(), number, fuel.getPrice(), fuel.getType()));
		}
		
		List<Fuel> allClientFuel = client.getClientFuel();
		foundSimilar = false;
		Fuel existingClientFuel = null;
		for (Fuel clientFuel : allClientFuel) {
			if (clientFuel.getName().equals(fuel.getName()) && fuel.getType() == fuel.getType()) {
				clientFuel.setNumber(clientFuel.getNumber() + number);
				fuel.setNumber(fuel.getNumber() - number);
				foundSimilar = true;
				existingClientFuel = clientFuel;
			}
		}
		if (foundSimilar) {
			return existingClientFuel;
		}
		
		Fuel newClientFuel = new Fuel(fuel.getName(), number, fuel.getPrice(), fuel.getType());
		allClientFuel.add(newClientFuel);
		container.getCashBox().setEarningsForFuel(container.getCashBox().getEarningsForFuel() + fuel.getPrice() * number);
		return newClientFuel;
	}
	
}
