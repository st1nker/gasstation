package services;

import java.util.List;

import initializers.Container;
import models.CashBox;
import models.Fuel;
import models.Product;

public class AccountantService {
	Container container;
	
	public void setContainer(Container container) {
		this.container = container;
	}
	
	public void addProduct(Product newProduct) {
		List<Product> allProducts = container.getCashBox().getProducts();
		boolean foundSimilar = false;
		for (Product product : allProducts) {
			if (product.getName().equals(newProduct.getName()) && product.getType() == newProduct.getType()) {
				product.setPrice(newProduct.getPrice());
				product.setNumber(product.getNumber() + newProduct.getNumber());
				foundSimilar = true;
			}
		}
		if (!foundSimilar) {
			allProducts.add(newProduct);
		}
	}
	
	public void addFuel(Fuel newFuel) {
		List<Fuel> allFuel = container.getCashBox().getFuel();
		boolean foundSimilar = false;
		for (Fuel fuel : allFuel) {
			if (fuel.getName().equals(newFuel.getName()) && fuel.getType() == newFuel.getType()) {
				fuel.setPrice(newFuel.getPrice());
				fuel.setNumber(fuel.getNumber() + newFuel.getNumber());
				foundSimilar = true;
			}
		}
		if (!foundSimilar) {
			allFuel.add(newFuel);
		}
	}
	
	public void collectRevenue() {
		CashBox cashBox = container.getCashBox();
		cashBox.setEarningsForFuel(0);
		cashBox.setEarningsForProducts(0);
	}
	
	public void setProductPrice(int newPrice, Product product) {
		product.setPrice(newPrice);
	}
	
	public void setProductPrice(int newPrice, Fuel fuel) {
		fuel.setPrice(newPrice);
	}
}
