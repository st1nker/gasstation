package services;

import java.util.ArrayList;
import java.util.List;

import initializers.Container;
import models.Column;
import models.Fuel;
import models.Vehicle;

public class ColumnService {
	Container container;
	List<Column> columns = new ArrayList<>();
	
	public List<Column> getColumns() {
		return columns;
	}
	
	public void setContainer(Container container) {
		this.container = container;
	}
	
	public void addColumn(Column column) {
		columns.add(column);
	}
	
	public void fillUpVehicle(Fuel fuel) {
		Vehicle vehicle = container.getClientController().getClient().getVehicle();
		vehicle.setTankFullness(vehicle.getTankFullness() + fuel.getNumber());
	}
	
	public void closeColumn(Column column) {
		column.setClose(true);
	}
	
	public Column getColumn(int number) {
		return columns.get(number);
	}
	
}
