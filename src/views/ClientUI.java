package views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import initializers.Container;
import models.Column;
import models.Fuel;
import models.FuelType;
import models.Product;
import models.ProductType;

public class ClientUI {
	Container container;
	
	public void setContainer(Container container) {
		this.container = container;
	}
	
	public void createInterface() {
		Shell shell = new Shell(container.getDisplay(), SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setSize(1000, 800);
		
		GridLayout shellLayout = new GridLayout(3, false);
		shell.setLayout(shellLayout);
		
		initializeMenu(shell);
		
		shell.open();
	}
	
	public void changeMode() {
		container.getOwnerUI().createInterface();
	}
	
	public void buyProductHandler(Shell shell, Combo selectProductInput, Text numberInput) {
		String numberStr =	numberInput.getText();
		String productIdth = selectProductInput.getText();
		
		if (numberStr != "" && productIdth != selectProductInput.getItem(0)) {
			String[] productAttributes = productIdth.split(" ");
			String productName = productAttributes[1];
			int number = Integer.parseInt(numberStr);
			ProductType productType = productTypeCast(productAttributes[0]);
			
			for(Product product : container.getCashBox().getProducts()) {
				if (product.getName().equals(productName) && product.getType() == productType) {
					int finalPrice = product.getPrice() * number;
					if ( finalPrice > container.getClientController().getClient().getMoney()) {
						MessageBox alert = new MessageBox(shell, SWT.ICON_ERROR);
						alert.setText("Ошибка");
						alert.setMessage("Вам не хватает денежных средств, необходимо - " + Integer.toString(finalPrice));
						alert.open();
						return;
					}
					
					if (product.getNumber() < number) {
						MessageBox alert = new MessageBox(shell, SWT.ICON_ERROR);
						alert.setText("Ошибка");
						alert.setMessage("Недостаточно товара на складе, осталось товара - " + Integer.toString(product.getNumber()));
						alert.open();
						return;
					}
					
					container.getClientController().buyProduct(product, number);
					numberInput.setText("");
					selectProductInput.select(0);
					MessageBox alert = new MessageBox(shell, SWT.ICON_INFORMATION);
					alert.setText("Успех");
					alert.setMessage("Продукт куплен, фильнальная цена - " + Integer.toString(finalPrice));
					alert.open();
				}
			}; 
		} else {
			MessageBox alert = new MessageBox(shell, SWT.ICON_ERROR);
			alert.setText("Ошибка");
			alert.setMessage("Выберите продукт и введите количество");
			alert.open();
		}
	}
	
	public void buyFuelHandler(Shell shell, Combo selectFuelInput, Combo selectColumnInput, Text numberInput) {
		String numberStr =	numberInput.getText();
		String fuelIdth = selectFuelInput.getText();
		String columnNumberStr = selectColumnInput.getText();
		
		if (numberStr != "" && fuelIdth != selectFuelInput.getItem(0) && columnNumberStr != selectColumnInput.getItem(0)) {
			String[] fuelAttributes = fuelIdth.split(" ");
			String fuelName = fuelAttributes[1];
			int number = Integer.parseInt(numberStr);
			FuelType fuelType = fuelTypeCast(fuelAttributes[0]);
			
			container.getCashBox().getFuel().forEach((fuel) -> {
				if (fuel.getName().equals(fuelName) && fuel.getType() == fuelType) {
					int finalPrice = fuel.getPrice() * number;
					if ( finalPrice > container.getClientController().getClient().getMoney()) {
						MessageBox alert = new MessageBox(shell, SWT.ICON_ERROR);
						alert.setText("Ошибка");
						alert.setMessage("Вам не хватает денежных средств, необходимо - " + Integer.toString(finalPrice));
						alert.open();
						return;
					}
					
					if (fuel.getNumber() < number) {
						MessageBox alert = new MessageBox(shell, SWT.ICON_ERROR);
						alert.setText("Ошибка");
						alert.setMessage("Недостаточно топлива на складе, осталось товара - " + Integer.toString(fuel.getNumber()));
						alert.open();
						return;
					}
					
					container.getClientController().buyFuel(fuel, columnNumberStr, number);
					numberInput.setText("");
					selectFuelInput.select(0);
					MessageBox alert = new MessageBox(shell, SWT.ICON_INFORMATION);
					alert.setText("Успех");
					alert.setMessage("Топливо куплено, фильнальная цена - " + Integer.toString(finalPrice));
					alert.open();
				}
			}); 
		} else {
			MessageBox alert = new MessageBox(shell, SWT.ICON_ERROR);
			alert.setText("Ошибка");
			alert.setMessage("Выберите топливо и введите количество");
			alert.open();
		}
	}
	
	public void fillUpVehicleHandler(Shell shell, Combo selectColumnInput, Combo selectFuelInput) {
		String columnNumberStr = selectColumnInput.getText();
		String fuelIdth = selectFuelInput.getText();
		
		if (columnNumberStr != selectColumnInput.getItem(0) && fuelIdth != selectFuelInput.getItem(0)) {
			container.getClientController().fillUpVehicle(columnNumberStr, fuelIdth);
			selectColumnInput.select(0);
			selectFuelInput.setItems(new String[] { "Выбирите топливо" });
			selectFuelInput.select(0);
			selectFuelInput.setEnabled(false);
			
			MessageBox alert = new MessageBox(shell, SWT.ICON_INFORMATION);
			alert.setText("Успех");
			alert.setMessage("Транспортное средство заправлено");
			alert.open();
		} else {
			MessageBox alert = new MessageBox(shell, SWT.ICON_ERROR);
			alert.setText("Ошибка");
			alert.setMessage("Выберите колонку и затем топливо");
			alert.open();
		}
	}
	
	// PRIVATE METHODS
	void initializeMenu(Shell shell) {
		GridData gridData = new GridData(350, 40);
		Composite header = createHeader(shell, false, null, null);
		Composite clientInfoGroup = createClientInfoGroup(shell, 100);
		Composite menuGroup = createCommonGroup(shell, "Меню");
		
		Button buyProductButton = new Button(menuGroup, SWT.PUSH);
		buyProductButton.setText("Купить товар");
		buyProductButton.setLayoutData(gridData);
		buyProductButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
	            header.dispose();
	            clientInfoGroup.dispose();
	            menuGroup.dispose();
	            createBuyProductForm(shell);
	        }
	    });
		
		Button buyFuelButton = new Button(menuGroup, SWT.PUSH);
		buyFuelButton.setText("Купить топливо");
		buyFuelButton.setLayoutData(gridData);
		buyFuelButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				 header.dispose();
				 clientInfoGroup.dispose();
		         menuGroup.dispose();
		         createBuyFuelForm(shell);
	        }
	    });
		
		Button fillUpVehicleButton = new Button(menuGroup, SWT.PUSH);
		fillUpVehicleButton.setText("Заправить Т/С");
		fillUpVehicleButton.setLayoutData(gridData);
		fillUpVehicleButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				header.dispose();
				clientInfoGroup.dispose();
		        menuGroup.dispose();
		        createFillUpVehicleForm(shell);
	        }
	    });
	}
	
	Composite createHeader(Shell shell, boolean withBackButton, Composite currentGroup, Composite clientInfoGroup) {
		Composite header = new Composite(shell, SWT.NONE);
		GridData headerData = new GridData();
		headerData.horizontalSpan = 3;
		header.setLayoutData(headerData);
		header.setLayout(new GridLayout(3, false));
		
		if (withBackButton) {
			Button backButton = new Button(header, SWT.PUSH);
			backButton.setText("Назад в меню");
			backButton.addSelectionListener(new SelectionAdapter() {
	            public void widgetSelected(SelectionEvent arg0) {
	            	if (currentGroup != null) {
	            		header.dispose();
	            		clientInfoGroup.dispose();
	            		currentGroup.dispose();
	            		initializeMenu(shell);
	            		shell.layout(true, true);
	            	}
	            }
	        });
			
			GridData gridDataForBackButton = new GridData(150, 50);
			gridDataForBackButton.horizontalAlignment = SWT.BEGINNING;
			backButton.setLayoutData(gridDataForBackButton);
		}
			
		Label label = new Label(header, SWT.NONE);
		label.setText("Вы вошли как владелец");
		GridData gridDataForLabel = new GridData();
		gridDataForLabel.horizontalAlignment = SWT.CENTER;
		gridDataForLabel.grabExcessHorizontalSpace = true;
		int indent;
		if (withBackButton) {
			indent = 450;
		} else {
			indent = 607;
		};
		gridDataForLabel.horizontalIndent = indent;
		label.setLayoutData(gridDataForLabel);
		
		Button changeModeButton = new Button(header, SWT.PUSH);
		changeModeButton.setText("Сменить режим");
		changeModeButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
            	shell.dispose();
            	changeMode();
            }
        });
		GridData gridDataForChangeModeButton = new GridData(150, 50);
		gridDataForChangeModeButton.horizontalAlignment = SWT.END;
		gridDataForChangeModeButton.horizontalIndent = 20;
		changeModeButton.setLayoutData(gridDataForChangeModeButton);
		
		return header;
	}
	
	Composite createCommonGroup(Shell shell, String name) {
		Composite group = new Composite(shell, SWT.NONE);
		
		GridData gridDataForGroup = new GridData();
		gridDataForGroup.horizontalSpan = 1;
		gridDataForGroup.verticalIndent = 100;
		gridDataForGroup.horizontalIndent = 100;
		gridDataForGroup.horizontalAlignment = SWT.CENTER;
		group.setLayoutData(gridDataForGroup);
		
		GridLayout layoutGroup = new GridLayout(1, true);
		layoutGroup.verticalSpacing = 25;
		group.setLayout(layoutGroup);
			
		Label label = new Label(group, SWT.NONE);
		
		label.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_CENTER));
		FontData[] fD = label.getFont().getFontData();
		fD[0].setHeight(16);
		label.setFont( new Font(shell.getDisplay(),fD[0]));
		label.setText(name);
		
		return group;
	}
	
	Composite createClientInfoGroup(Shell shell, int verticalIndent) {
		Composite infoGroup = new Composite(shell, SWT.NONE);
		GridData gridDataForGroup = new GridData();
		gridDataForGroup.horizontalSpan = 1;
		gridDataForGroup.verticalIndent = verticalIndent;
		infoGroup.setLayoutData(gridDataForGroup);
		
		GridLayout layoutGroup = new GridLayout(2, true);
		layoutGroup.verticalSpacing = 25;
		layoutGroup.horizontalSpacing = 25;
		infoGroup.setLayout(layoutGroup);
			
		Label labelName = new Label(infoGroup, SWT.NONE);
		labelName.setLayoutData(new GridData());
		FontData[] fD = labelName.getFont().getFontData();
		fD[0].setHeight(16);
		labelName.setFont( new Font(shell.getDisplay(),fD[0]));
		labelName.setText("Имя:");
		
		Label labelClientName = new Label(infoGroup, SWT.NONE);
		labelClientName.setLayoutData(new GridData());
		FontData[] fD2 = labelClientName.getFont().getFontData();
		fD2[0].setHeight(14);
		labelClientName.setFont( new Font(shell.getDisplay(),fD2[0]));
		labelClientName.setText(container.getClientController().getClient().getName());
		
		Label labelMoney = new Label(infoGroup, SWT.NONE);
		labelMoney.setLayoutData(new GridData());
		labelMoney.setFont( new Font(shell.getDisplay(),fD[0]));
		labelMoney.setText("Деньги:");
		
		Label labelClientMoney = new Label(infoGroup, SWT.NONE);
		labelClientMoney.setLayoutData(new GridData());
		labelClientMoney.setFont( new Font(shell.getDisplay(),fD2[0]));
		labelClientMoney.setText(Integer.toString(container.getClientController().getClient().getMoney()) + "$");
		return infoGroup;
	}
	
	VerifyListener onlyNumber() {
		return new VerifyListener() {
			public void verifyText(VerifyEvent e) {
		            Text text = (Text)e.getSource();
		            final String oldS = text.getText();
		            String newS = oldS.substring(0, e.start) + e.text + oldS.substring(e.end);
		            boolean isInt = true;
		            try
		            {
		                Integer.parseInt(newS);
		            }
		            catch(NumberFormatException ex)
		            {
		                isInt = false;
		            }
		            if((!isInt && !newS.equals("")) || newS.equals("0"))
		                e.doit = false;
		    };
		};
	}
	
	
	void createBuyProductForm(Shell shell) {
		Composite clientInfoGroup = createClientInfoGroup(new Shell(shell.getDisplay()), 115);
		Composite buyProductForm = createCommonGroup(new Shell(shell.getDisplay()), "Купить товар");
		Composite header = createHeader(shell, true, buyProductForm, clientInfoGroup);
		clientInfoGroup.setParent(shell);
		buyProductForm.setParent(shell);
		GridData gridData = new GridData(350, 30);
		
		Combo selectProductInput = new Combo(buyProductForm, SWT.DROP_DOWN | SWT.READ_ONLY);
		selectProductInput.setLayoutData(gridData);
		List<String> products = new ArrayList<>();
		products.add("Выберите товар");
		for (Product product : container.getCashBox().getProducts()) {
				products.add(productTypeToString(product.getType()) + " " + product.getName());
 		}
		
		String[] itemsArray = new String[products.size()];

		selectProductInput.setItems(products.toArray(itemsArray));
		selectProductInput.select(0);
		
		Text numberInput = new Text(buyProductForm, SWT.NONE);
		numberInput.setMessage("Количество...");
		numberInput.setLayoutData(gridData);
		numberInput.addVerifyListener(onlyNumber());
		
		Button submitButton = new Button(buyProductForm, SWT.PUSH);
		GridData submitButtonGridData = new GridData(200, 40);
		submitButtonGridData.horizontalAlignment = SWT.CENTER;
		submitButton.setLayoutData(submitButtonGridData);
		submitButton.setText("Купить товар");
		submitButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				buyProductHandler(shell, selectProductInput, numberInput);
				clientInfoGroup.dispose();
				buyProductForm.dispose();
				header.dispose();
				createBuyFuelForm(shell);
	        }
	    });
		
		shell.layout(true, true);
	}
	
	void createBuyFuelForm(Shell shell) {
		Composite clientInfoGroup = createClientInfoGroup(new Shell(shell.getDisplay()), 57);
		Composite buyFuelForm = createCommonGroup(new Shell(shell.getDisplay()), "Купить топливо");
		Composite header = createHeader(shell, true, buyFuelForm, clientInfoGroup);
		clientInfoGroup.setParent(shell);
		buyFuelForm.setParent(shell);
		GridData gridData = new GridData(350, 30);
		
		Combo selectFuelInput = new Combo(buyFuelForm, SWT.DROP_DOWN | SWT.READ_ONLY);
		selectFuelInput.setLayoutData(gridData);
		List<String> allFuel = new ArrayList<>();
		allFuel.add("Выберите топливо");
		for (Fuel fuel : container.getCashBox().getFuel()) {
				allFuel.add(fuelTypeToString(fuel.getType()) + " " + fuel.getName());
 		}
		
		String[] itemsArray = new String[allFuel.size()];

		selectFuelInput.setItems(allFuel.toArray(itemsArray));
		selectFuelInput.select(0);
		
		Combo selectColumnInput = new Combo(buyFuelForm, SWT.DROP_DOWN | SWT.READ_ONLY);
		selectColumnInput.setLayoutData(gridData);
		List<String> columnNumbers = new ArrayList<>();
		columnNumbers.add("Выберите колонку");
		for (Column column : container.getColumnService().getColumns()) {
			if (!column.isClose()) {
				columnNumbers.add(Integer.toString(column.getNumber()));
			}
		}
		String[] columnsArray = new String[columnNumbers.size()];
		selectColumnInput.setItems(columnNumbers.toArray(columnsArray));
		selectColumnInput.select(0);
		
		Text numberInput = new Text(buyFuelForm, SWT.NONE);
		numberInput.setMessage("Количество...");
		numberInput.setLayoutData(gridData);
		numberInput.addVerifyListener(onlyNumber());
		
		Button submitButton = new Button(buyFuelForm, SWT.PUSH);
		GridData submitButtonGridData = new GridData(200, 40);
		submitButtonGridData.horizontalAlignment = SWT.CENTER;
		submitButton.setLayoutData(submitButtonGridData);
		submitButton.setText("Купить топливо");
		submitButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				buyFuelHandler(shell, selectFuelInput, selectColumnInput, numberInput);
				clientInfoGroup.dispose();
				buyFuelForm.dispose();
				header.dispose();
				createBuyFuelForm(shell);
	        }
	    });
		
		shell.layout(true, true);
	}
	
	void createFillUpVehicleForm(Shell shell) {
		Composite clientInfoGroup = createClientInfoGroup(new Shell(shell.getDisplay()), 124);
		Composite fillUpVehicleForm = createCommonGroup(new Shell(shell.getDisplay()), "Заправить");
		createHeader(shell, true, fillUpVehicleForm, clientInfoGroup);
		clientInfoGroup.setParent(shell);
		fillUpVehicleForm.setParent(shell);
		GridData gridData = new GridData(350, 30);
		
		Combo selectColumnInput = new Combo(fillUpVehicleForm, SWT.DROP_DOWN | SWT.READ_ONLY);
		selectColumnInput.setLayoutData(gridData);
		List<String> columnNumbers = new ArrayList<>();
		columnNumbers.add("Выберите колонку");
		for (Column column : container.getColumnService().getColumns()) {
			if (!column.isClose()) {
				columnNumbers.add(Integer.toString(column.getNumber()));
			}
		}
		String[] itemsArray = new String[columnNumbers.size()];
		selectColumnInput.setItems(columnNumbers.toArray(itemsArray));
		selectColumnInput.select(0);
		
		Combo selectFuelInput = new Combo(fillUpVehicleForm, SWT.DROP_DOWN | SWT.READ_ONLY);
		selectFuelInput.setLayoutData(gridData);
		selectFuelInput.setItems(new String[] { "Выберите топливо" });
		selectFuelInput.select(0);
		selectFuelInput.setEnabled(false);
		
		selectColumnInput.addSelectionListener(new SelectionAdapter() {
		      public void widgetSelected(SelectionEvent e) {
		    	  String columnNumberStr = selectColumnInput.getText();
		    	  if ( columnNumberStr == selectColumnInput.getItem(0)) {
		    		  String[] defaultItems = new String[] { "Выберите топливо" };
		    		  selectFuelInput.setItems(defaultItems);
		    		  selectFuelInput.setEnabled(false);
		    		  return;
		    	  }
		    	  int columnNumber = Integer.parseInt(columnNumberStr);
		    	  Column column = null;
		    	  for (Column columnIterator : container.getColumnService().getColumns()) {
		    		  if (columnIterator.getNumber() == columnNumber) {
		    			  column = columnIterator;
		    			  break;
		    		  }
		    	  }
		    	  
		    	  List<String> items = new ArrayList<>();
	    		  items.add("Выберите топливо");
		    	  for (Fuel fuel : column.getAllFuel()) {
		    		  items.add(fuelTypeToString(fuel.getType()) + " " + fuel.getName());
		    	  }
		    	  String[] itemsArray = new String[items.size()];
		    	  selectFuelInput.setItems(items.toArray(itemsArray));
		    	  selectFuelInput.select(0);
		    	  selectFuelInput.setEnabled(true);
		    	  selectFuelInput.layout(true, true);
		      }
		});
		
		Button submitButton = new Button(fillUpVehicleForm, SWT.PUSH);
		GridData submitButtonGridData = new GridData(300, 40);
		submitButtonGridData.horizontalAlignment = SWT.CENTER;
		submitButton.setLayoutData(submitButtonGridData);
		submitButton.setText("Заправить транспортное средство");
		submitButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				fillUpVehicleHandler(shell, selectColumnInput, selectFuelInput);
	        }
	    });
		
		shell.layout(true, true);
	}
	
	String productTypeToString(ProductType type) {
		switch(type) {
		case Food:
			return "Еда";
		case Instruments:
			return "Инструменты";
		case Appliences:
			return "Приборы";
		}
		
		return null;
	}
	
	String fuelTypeToString(FuelType type) {
		switch(type) {
		case AI_95:
			return "АИ-95";
		
		case AI_92:
			return "АИ-92";
		
		case Diesel:
			return "Дизель";
		}
		
		return null;
	}
	
	ProductType productTypeCast(String value) {
		switch(value) {
		case "Еда":
			return ProductType.Food;
		
		case "Инструменты":
			return ProductType.Instruments;
		
		case "Приборы":
			return ProductType.Appliences;
		}
		
		return null;
	}
	
	FuelType fuelTypeCast(String value) {
		switch(value) {
		case "АИ-95":
			return FuelType.AI_95;
		
		case "АИ-92":
			return FuelType.AI_92;
		
		case "Дизель":
			return FuelType.Diesel;
		}
		
		return null;
	}
}
