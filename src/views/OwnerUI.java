package views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import initializers.Container;
import models.CashBox;
import models.Column;
import models.Fuel;
import models.FuelType;
import models.Product;
import models.ProductType;

public class OwnerUI {
	Container container;
	
	public void setContainer(Container container) {
		this.container = container;
	}
	
	public void createInterface() {
		Shell shell = new Shell(container.getDisplay(), SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setSize(1000, 800);
		
		GridLayout shellLayout = new GridLayout(3, false);
		shell.setLayout(shellLayout);
		
		initializeMenu(shell);
		
		shell.open();
	}
	
	public void changeMode() {
		container.getClientUI().createInterface();
	}
	
	public void closeColumnHandler(Shell shell, Combo selectColumnInput) {
		String columnNumberStr = selectColumnInput.getText();
		if ( columnNumberStr == selectColumnInput.getItem(0)) {
			MessageBox alert = new MessageBox(shell, SWT.ICON_ERROR);
			alert.setText("Ошибка");
			alert.setMessage("Выберите колонку для закрытия");
			alert.open();
		} else {
			container.getOwnerController().closeColumn(columnNumberStr);
			selectColumnInput.remove(columnNumberStr);
			selectColumnInput.select(0);
			MessageBox alert = new MessageBox(shell, SWT.ICON_INFORMATION);
			alert.setText("Успех");
			alert.setMessage("Колонка номер " + columnNumberStr + " закрыта");
			alert.open();
		}
	}
	
	public void addFuelHandler(Shell shell, Text nameInput, Text priceInput, Text numberInput, Combo typeInput) {
		String name = nameInput.getText();
		String price =	priceInput.getText();
		String number = numberInput.getText();
		String type =	typeInput.getText();
		
		if (name != "" && price != "" && number != "" && type != typeInput.getItem(0)) {
			container.getOwnerController().addFuel(name, price, number, type);
			
			nameInput.setText("");
			priceInput.setText("");
			numberInput.setText("");
			typeInput.select(0);
			MessageBox alert = new MessageBox(shell, SWT.ICON_INFORMATION);
			alert.setText("Успех");
			alert.setMessage("Топливо добавлено");
			alert.open();
		} else {
			MessageBox alert = new MessageBox(shell, SWT.ICON_ERROR);
			alert.setText("Ошибка");
			alert.setMessage("Для добавления топлива необходимо заполнить все поля формы");
			alert.open();
		}
	}
	
	public void addProductHandler(Shell shell, Text nameInput, Text priceInput, Text numberInput, Combo typeInput) {
		String name = nameInput.getText();
		String price =	priceInput.getText();
		String number = numberInput.getText();
		String type = typeInput.getText();
		
		if (name != "" && price != "" && number != "" && type != typeInput.getItem(0)) {
			container.getOwnerController().addProduct(name, price, number, type);
			
			nameInput.setText("");
			priceInput.setText("");
			numberInput.setText("");
			typeInput.select(0);
			MessageBox alert = new MessageBox(shell, SWT.ICON_INFORMATION);
			alert.setText("Успех");
			alert.setMessage("Продукт добавлен");
			alert.open();
		} else {
			MessageBox alert = new MessageBox(shell, SWT.ICON_ERROR);
			alert.setText("Ошибка");
			alert.setMessage("Для добавления продукта необходимо заполнить все поля формы");
			alert.open();
		}
	}
	
	public void addColumnHandler(Shell shell) {
		container.getOwnerController().addColumn();
		
		MessageBox alert = new MessageBox(shell, SWT.ICON_INFORMATION);
		alert.setText("Успех");
		List<Column> columns = container.getColumnService().getColumns();
		String columnNumberStr = Integer.toString(columns.size());
		int activeColumnNumber = 0;
		for (Column column : columns) {
			if (!column.isClose()) {
				activeColumnNumber += 1;
			};
		};
		
		alert.setMessage("Колонка добавлена.\nВсего колонок: " + columnNumberStr + "\n" + "Активных колонок: " + Integer.toString(activeColumnNumber));
		alert.open();
	}
	
	public void setProductPriceHandler(Shell shell, Combo selectProductInput, Text priceInput) {
		String price =	priceInput.getText();
		String productIdth = selectProductInput.getText();
		
		if (price != "" && productIdth != selectProductInput.getItem(0)) {
			String[] productAttributes = productIdth.split(" ");
			container.getOwnerController().setProductPrice(productAttributes[1], productAttributes[0], price);
			
			priceInput.setText("");
			selectProductInput.select(0);
			MessageBox alert = new MessageBox(shell, SWT.ICON_INFORMATION);
			alert.setText("Успех");
			alert.setMessage("Цена на продукт изменена");
			alert.open();
		} else {
			MessageBox alert = new MessageBox(shell, SWT.ICON_ERROR);
			alert.setText("Ошибка");
			alert.setMessage("Выберите продукт и введите новую цену");
			alert.open();
		}
	}
	
	public void setFuelPriceHandler(Shell shell, Combo selectFuelInput, Text priceInput) {
		String price =	priceInput.getText();
		String fuelIdth = selectFuelInput.getText();
		
		if (price != "" && fuelIdth != selectFuelInput.getItem(0)) {
			String[] fuelAttributes = fuelIdth.split(" ");
			container.getOwnerController().setFuelPrice(fuelAttributes[1], fuelAttributes[0], price);
			
			priceInput.setText("");
			selectFuelInput.select(0);
			MessageBox alert = new MessageBox(shell, SWT.ICON_INFORMATION);
			alert.setText("Успех");
			alert.setMessage("Цена на топливо изменена");
			alert.open();
		} else {
			MessageBox alert = new MessageBox(shell, SWT.ICON_ERROR);
			alert.setText("Ошибка");
			alert.setMessage("Выберите топливо и введите новую цену");
			alert.open();
		}
	}
	
	public void collectRevenueHandler(Shell shell) {
		CashBox cashBox = container.getCashBox();
		int earningForFuel = cashBox.getEarningsForFuel();
		int earningForProducts = cashBox.getEarningsForProducts();
		String revenueStr = Integer.toString(earningForProducts + earningForFuel);
		
		container.getOwnerController().collectRevenue();
		MessageBox alert = new MessageBox(shell, SWT.ICON_INFORMATION);
		alert.setMessage("Собрано за топливо: " + Integer.toString(earningForFuel) + "\nCобрано за товары: " + Integer.toString(earningForProducts) + "\nСобрано всего: " + revenueStr);
		alert.open();
	}
	
	// PRIVATE METHODS
	void initializeMenu(Shell shell) {		
		GridData gridData = new GridData(350, 40);
		Composite header = createHeader(shell, false, null);
		Composite menuGroup = createCommonGroup(shell, "Меню");
		
		Button addFuelButton = new Button(menuGroup, SWT.PUSH);
		addFuelButton.setText("Добавить топливо");
		addFuelButton.setLayoutData(gridData);
		addFuelButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				header.dispose();
				menuGroup.dispose();
	            createAddFuelForm(shell);
	        }
	    });
		
		Button addProductButton = new Button(menuGroup, SWT.PUSH);
		addProductButton.setText("Добавить товар");
		addProductButton.setLayoutData(gridData);
		addProductButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
	            header.dispose();
				menuGroup.dispose();
	            createAddProductForm(shell);
	        }
	    });
		
		Button addColumnButton = new Button(menuGroup, SWT.PUSH);
		addColumnButton.setText("Добавить колонку");
		addColumnButton.setLayoutData(gridData);
		addColumnButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				addColumnHandler(shell);
	        }
	    });
		
		Button closeColumn = new Button(menuGroup, SWT.PUSH);
		closeColumn.setText("Закрыть колонку");
		closeColumn.setLayoutData(gridData);
		closeColumn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				header.dispose();
				menuGroup.dispose();
	            createCloseColumnForm(shell);
	        }
	    });
		
		Button changePriceForProduct = new Button(menuGroup, SWT.PUSH);
		changePriceForProduct.setText("Изменить цену на товар");
		changePriceForProduct.setLayoutData(gridData);
		changePriceForProduct.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				header.dispose();
				menuGroup.dispose();
	            createSetProductPriceForm(shell);
	        }
	    });
		
		Button changePriceForFuel = new Button(menuGroup, SWT.PUSH);
		changePriceForFuel.setText("Изменить цену на топливо");
		changePriceForFuel.setLayoutData(gridData);
		changePriceForFuel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				header.dispose();
				menuGroup.dispose();
	            createSetFuelPriceForm(shell);
	        }
	    });
		
		Button collectRevenueButton  = new Button(menuGroup, SWT.PUSH);
		collectRevenueButton.setText("Собрать выручку");
		collectRevenueButton.setLayoutData(gridData);
		collectRevenueButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
	            collectRevenueHandler(shell);
	        }
	    });
	}
	
	Composite createHeader(Shell shell, boolean withBackButton, Composite currentGroup) {
		Composite header = new Composite(shell, SWT.NONE);
		GridData headerData = new GridData();
		headerData.horizontalSpan = 3;
		header.setLayoutData(headerData);
		header.setLayout(new GridLayout(3, false));
		
		if (withBackButton) {
			Button backButton = new Button(header, SWT.PUSH);
			backButton.setText("Назад в меню");
			backButton.addSelectionListener(new SelectionAdapter() {
	            public void widgetSelected(SelectionEvent arg0) {
	            	if (currentGroup != null) {
	            		header.dispose();
	            		currentGroup.dispose();
	            		initializeMenu(shell);
	            		shell.layout(true, true);
	            	}
	            }
	        });
			
			GridData gridDataForBackButton = new GridData(150, 50);
			gridDataForBackButton.horizontalAlignment = SWT.BEGINNING;
			backButton.setLayoutData(gridDataForBackButton);
		}
			
		Label label = new Label(header, SWT.NONE);
		label.setText("Вы вошли как владелец");
		GridData gridDataForLabel = new GridData();
		gridDataForLabel.horizontalAlignment = SWT.CENTER;
		gridDataForLabel.grabExcessHorizontalSpace = true;
		int indent;
		if (withBackButton) {
			indent = 450;
		} else {
			indent = 607;
		};
		gridDataForLabel.horizontalIndent = indent;
		label.setLayoutData(gridDataForLabel);
		
		Button changeModeButton = new Button(header, SWT.PUSH);
		changeModeButton.setText("Сменить режим");
		changeModeButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
            	shell.dispose();
            	changeMode();
            }
        });
		GridData gridDataForChangeModeButton = new GridData(150, 50);
		gridDataForChangeModeButton.horizontalAlignment = SWT.END;
		gridDataForChangeModeButton.horizontalIndent = 20;
		changeModeButton.setLayoutData(gridDataForChangeModeButton);
		
		return header;
	}
	
	Composite createCommonGroup(Shell shell, String name) {
		Composite group = new Composite(shell, SWT.NONE);
		
		GridData gridDataForGroup = new GridData();
		gridDataForGroup.horizontalSpan = 3;
		gridDataForGroup.verticalIndent = 100;
		gridDataForGroup.horizontalAlignment = SWT.CENTER;
		group.setLayoutData(gridDataForGroup);
		
		GridLayout layoutGroup = new GridLayout(1, true);
		layoutGroup.verticalSpacing = 25;
		group.setLayout(layoutGroup);
			
		Label label = new Label(group, SWT.NONE);
		
		label.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_CENTER));
		FontData[] fD = label.getFont().getFontData();
		fD[0].setHeight(16);
		label.setFont( new Font(shell.getDisplay(),fD[0]));
		label.setText(name);
		
		return group;
	}
	
	VerifyListener onlyNumber() {
		return new VerifyListener() {
			public void verifyText(VerifyEvent e) {
		            Text text = (Text)e.getSource();
		            final String oldS = text.getText();
		            String newS = oldS.substring(0, e.start) + e.text + oldS.substring(e.end);
		            boolean isInt = true;
		            try
		            {
		                Integer.parseInt(newS);
		            }
		            catch(NumberFormatException ex)
		            {
		                isInt = false;
		            }
		            if((!isInt && !newS.equals("")) || newS.equals("0"))
		                e.doit = false;
		    };
		};
	}
	
	void createAddFuelForm(Shell shell) {
		Composite addProductForm = createCommonGroup(new Shell(shell.getDisplay()), "Добавить топливо");
		createHeader(shell, true, addProductForm);
		addProductForm.setParent(shell);
		GridData gridData = new GridData(350, 30);
		
		Text nameInput = new Text(addProductForm, SWT.NONE);
		nameInput.setMessage("Название...");
		nameInput.setLayoutData(gridData);
		
		Text priceInput = new Text(addProductForm, SWT.NONE);
		priceInput.setMessage("Цена...");
		priceInput.setLayoutData(gridData);
		priceInput.addVerifyListener(onlyNumber());
		
		Text numberInput = new Text(addProductForm, SWT.NONE);
		numberInput.setMessage("Количество...");
		numberInput.setLayoutData(gridData);
		numberInput.addVerifyListener(onlyNumber());
		
		Combo typeInput = new Combo(addProductForm, SWT.DROP_DOWN | SWT.READ_ONLY);
		typeInput.setLayoutData(gridData);
		typeInput.setItems(new String[] { "Тип...", "АИ-95", "АИ-92", "Дизель" });
		typeInput.select(0);
		
		Button submitButton = new Button(addProductForm, SWT.PUSH);
		GridData submitButtonGridData = new GridData(200, 40);
		submitButtonGridData.horizontalAlignment = SWT.CENTER;
		submitButton.setLayoutData(submitButtonGridData);
		submitButton.setText("Добавить топливо");
		submitButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				addFuelHandler(shell, nameInput, priceInput, numberInput, typeInput);
	        }
	    });
		
		shell.layout(true, true);
	}
	
	void createAddProductForm(Shell shell) {
		Composite addProductForm = createCommonGroup(new Shell(shell.getDisplay()), "Добавить новый товар");
		createHeader(shell, true, addProductForm);
		addProductForm.setParent(shell);
		GridData gridData = new GridData(350, 30);
		
		Text nameInput = new Text(addProductForm, SWT.NONE);
		nameInput.setMessage("Название...");
		nameInput.setLayoutData(gridData);
		
		Text priceInput = new Text(addProductForm, SWT.NONE);
		priceInput.setMessage("Цена...");
		priceInput.setLayoutData(gridData);
		priceInput.addVerifyListener(onlyNumber());
		
		Text numberInput = new Text(addProductForm, SWT.NONE);
		numberInput.setMessage("Количество...");
		numberInput.setLayoutData(gridData);
		numberInput.addVerifyListener(onlyNumber());
		
		Combo typeInput = new Combo(addProductForm, SWT.DROP_DOWN | SWT.READ_ONLY);
		typeInput.setLayoutData(gridData);
		typeInput.setItems(new String[] { "Тип...", "Еда", "Инструменты", "Приборы" });
		typeInput.select(0);
		
		Button submitButton = new Button(addProductForm, SWT.PUSH);
		GridData submitButtonGridData = new GridData(200, 40);
		submitButtonGridData.horizontalAlignment = SWT.CENTER;
		submitButton.setLayoutData(submitButtonGridData);
		submitButton.setText("Добавить товар");
		submitButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				addProductHandler(shell, nameInput, priceInput, numberInput, typeInput);
	        }
	    });
		
		shell.layout(true, true);
	}
	
	void createCloseColumnForm(Shell shell) {
		Composite closeColumnForm = createCommonGroup(new Shell(shell.getDisplay()), "Закрытие колонки");
		createHeader(shell, true, closeColumnForm);
		closeColumnForm.setParent(shell);
		GridData gridData = new GridData(350, 30);
		
		Combo selectColumnInput = new Combo(closeColumnForm, SWT.DROP_DOWN | SWT.READ_ONLY);
		selectColumnInput.setLayoutData(gridData);
		List<String> columnNumbers = new ArrayList<>();
		columnNumbers.add("Выберите колонку");
		for (Column column : container.getColumnService().getColumns()) {
			if (!column.isClose()) {
				columnNumbers.add(Integer.toString(column.getNumber()));
			}
		}
		String[] itemsArray = new String[columnNumbers.size()];
		selectColumnInput.setItems(columnNumbers.toArray(itemsArray));
		selectColumnInput.select(0);
		
		Button submitButton = new Button(closeColumnForm, SWT.PUSH);
		GridData submitButtonGridData = new GridData(200, 40);
		submitButtonGridData.horizontalAlignment = SWT.CENTER;
		submitButton.setLayoutData(submitButtonGridData);
		submitButton.setText("Закрыть колонку");
		submitButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				closeColumnHandler(shell, selectColumnInput);
	        }
	    });
		
		shell.layout(true, true);
	}
	
	void createSetProductPriceForm(Shell shell) {
		Composite setProductPriceForm = createCommonGroup(new Shell(shell.getDisplay()), "Изменение цены на товар");
		createHeader(shell, true, setProductPriceForm);
		setProductPriceForm.setParent(shell);
		GridData gridData = new GridData(350, 30);
		
		Combo selectProductInput = new Combo(setProductPriceForm, SWT.DROP_DOWN | SWT.READ_ONLY);
		selectProductInput.setLayoutData(gridData);
		List<String> products = new ArrayList<>();
		products.add("Выберите товар");
		for (Product product : container.getCashBox().getProducts()) {
				products.add(productTypeToString(product.getType()) + " " + product.getName());
 		}
		
		String[] itemsArray = new String[products.size()];

		selectProductInput.setItems(products.toArray(itemsArray));
		selectProductInput.select(0);
		
		Text priceInput = new Text(setProductPriceForm, SWT.NONE);
		priceInput.setMessage("Введите цену...");
		priceInput.setLayoutData(gridData);
		priceInput.addVerifyListener(onlyNumber());
		
		Button submitButton = new Button(setProductPriceForm, SWT.PUSH);
		GridData submitButtonGridData = new GridData(200, 40);
		submitButtonGridData.horizontalAlignment = SWT.CENTER;
		submitButton.setLayoutData(submitButtonGridData);
		submitButton.setText("Установить цену");
		submitButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				setProductPriceHandler(shell, selectProductInput, priceInput);
	        }
	    });
		
		shell.layout(true, true);
	}
	
	void createSetFuelPriceForm(Shell shell) {
		Composite setFuelPriceForm = createCommonGroup(new Shell(shell.getDisplay()), "Изменение цены на топливо");
		createHeader(shell, true, setFuelPriceForm);
		setFuelPriceForm.setParent(shell);
		GridData gridData = new GridData(350, 30);
		
		Combo selectFuelInput = new Combo(setFuelPriceForm, SWT.DROP_DOWN | SWT.READ_ONLY);
		selectFuelInput.setLayoutData(gridData);
		List<String> allFuel = new ArrayList<>();
		allFuel.add("Выберите топливо");
		for (Fuel fuel : container.getCashBox().getFuel()) {
				allFuel.add(fuelTypeToString(fuel.getType()) + " " + fuel.getName());
 		}
		
		String[] itemsArray = new String[allFuel.size()];

		selectFuelInput.setItems(allFuel.toArray(itemsArray));
		selectFuelInput.select(0);
		
		Text priceInput = new Text(setFuelPriceForm, SWT.NONE);
		priceInput.setMessage("Введите цену...");
		priceInput.setLayoutData(gridData);
		priceInput.addVerifyListener(onlyNumber());
		
		Button submitButton = new Button(setFuelPriceForm, SWT.PUSH);
		GridData submitButtonGridData = new GridData(200, 40);
		submitButtonGridData.horizontalAlignment = SWT.CENTER;
		submitButton.setLayoutData(submitButtonGridData);
		submitButton.setText("Установить цену");
		submitButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				setFuelPriceHandler(shell, selectFuelInput, priceInput);
	        }
	    });
		
		shell.layout(true, true);
	}
	
	String productTypeToString(ProductType type) {
		switch(type) {
		case Food:
			return "Еда";
		case Instruments:
			return "Инструменты";
		case Appliences:
			return "Приборы";
		}
		
		return null;
	}
	
	String fuelTypeToString(FuelType type) {
		switch(type) {
		case AI_95:
			return "АИ-95";
		
		case AI_92:
			return "АИ-92";
		
		case Diesel:
			return "Дизель";
		}
		
		return null;
	}
}
