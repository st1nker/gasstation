package controllers;

import initializers.Container;
import models.Column;
import models.Fuel;
import models.FuelType;
import models.Product;
import models.ProductType;

public class OwnerController {
	Container container;
	
	public void setContainer(Container container) {
		this.container = container;
	}
	
	public void addProduct(String name, String price, String number, String type) {
		container.getAccountantService().addProduct(new Product(name, Integer.parseInt(number), Integer.parseInt(price), productTypeCast(type)));
	}
	
	public void addFuel(String name, String price, String number, String type) {
		container.getAccountantService().addFuel(new Fuel(name, Integer.parseInt(number), Integer.parseInt(price), fuelTypeCast(type)));
	}
	
	public void addColumn() {
		int columnNumber = container.getColumnService().getColumns().size() + 1;
		container.getColumnService().addColumn(new Column(columnNumber));
	}
	
	public void collectRevenue() {
		container.getAccountantService().collectRevenue();
	}
	
	public void closeColumn(String columnNumber) {
		Column column = null;
		for (Column columnIterator : container.getColumnService().getColumns()) {
			if (columnIterator.getNumber() == Integer.parseInt(columnNumber)) {
				column = columnIterator;
				break;
			}
		}
		
		container.getColumnService().closeColumn(column);
	}
	
	public void setProductPrice(String productName, String productType, String newPrice) {
		container.getCashBox().getProducts().forEach((product) -> {
			if (product.getName() == productName && product.getType() == productTypeCast(productType)) {
				container.getAccountantService().setProductPrice(Integer.parseInt(newPrice), product);
			}
		}); 
	}
	
	public void setFuelPrice(String fuelName, String fuelType, String newPrice) {
		container.getCashBox().getFuel().forEach((fuel) -> {
			if (fuel.getName() == fuelName && fuel.getType() == fuelTypeCast(fuelType)) {
				container.getAccountantService().setProductPrice(Integer.parseInt(newPrice), fuel);
			}
		}); 
	}
	
	
	ProductType productTypeCast(String value) {
		switch(value) {
		case "Еда":
			return ProductType.Food;
		
		case "Инструменты":
			return ProductType.Instruments;
		
		case "Приборы":
			return ProductType.Appliences;
		}
		
		return null;
	}
	
	FuelType fuelTypeCast(String value) {
		switch(value) {
		case "АИ-95":
			return FuelType.AI_95;
		
		case "АИ-92":
			return FuelType.AI_92;
		
		case "Дизель":
			return FuelType.Diesel;
		}
		
		return null;
	}
}
