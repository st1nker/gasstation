package controllers;

import java.util.List;

import initializers.Container;
import models.Client;
import models.Column;
import models.Fuel;
import models.FuelType;
import models.Product;

public class ClientController {
	Container container;
	Client client;
	
	public ClientController(Client client) {
		this.client = client;
	}
	
	public void setContainer(Container container) {
		this.container = container;
	}
	
	public Client getClient() {
		return client;
	}

	public void buyProduct(Product product, int number) {
		container.getCashBoxService().sellProduct(product, number);
	}

	public void buyFuel(Fuel fuel, String columnNumberStr, int number) {
		Column column = null;
		for (Column columnIterator : container.getColumnService().getColumns()) {
			if (Integer.parseInt(columnNumberStr) == columnIterator.getNumber()) {
				column = columnIterator;
			}
		}
		container.getCashBoxService().sellFuel(column, fuel, number);
		
	}

	public void fillUpVehicle(String columnNumberStr, String fuelIdth) {
		int columnNumber = Integer.parseInt(columnNumberStr);
		
		Column column = null;
		for (Column columnIterator : container.getColumnService().getColumns()) {
  		  if (columnIterator.getNumber() == columnNumber) {
  			  column = columnIterator;
  			  break;
  		  }
  	  	}
		
		String[] fuelAttributes = fuelIdth.split(" ");
		String fuelName = fuelAttributes[1];
		FuelType fuelType = fuelTypeCast(fuelAttributes[0]);
		
		Fuel fuel = null;
		List<Fuel> allColumnFuel = column.getAllFuel();
		for (Fuel fuelIterator : allColumnFuel) {
			if (fuelIterator.getName().equals(fuelName) && fuelIterator.getType() == fuelType) {
				fuel = fuelIterator;
			}
  	  	}
		allColumnFuel.remove(fuel);
		
		container.getColumnService().fillUpVehicle(fuel);
	}
	
	FuelType fuelTypeCast(String value) {
		switch(value) {
		case "АИ-95":
			return FuelType.AI_95;
		
		case "АИ-92":
			return FuelType.AI_92;
		
		case "Дизель":
			return FuelType.Diesel;
		}
		
		return null;
	}
}
